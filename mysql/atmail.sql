-- MySQL dump 10.13  Distrib 5.1.52, for unknown-linux-gnu (x86_64)
--
-- Host: localhost    Database: atmail
-- ------------------------------------------------------
-- Server version	5.1.52-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AbookGroup_a`
--

DROP TABLE IF EXISTS `AbookGroup_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_a` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_a`
--

LOCK TABLES `AbookGroup_a` WRITE;
/*!40000 ALTER TABLE `AbookGroup_a` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_b`
--

DROP TABLE IF EXISTS `AbookGroup_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_b` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_b`
--

LOCK TABLES `AbookGroup_b` WRITE;
/*!40000 ALTER TABLE `AbookGroup_b` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_b` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_c`
--

DROP TABLE IF EXISTS `AbookGroup_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_c` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_c`
--

LOCK TABLES `AbookGroup_c` WRITE;
/*!40000 ALTER TABLE `AbookGroup_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_d`
--

DROP TABLE IF EXISTS `AbookGroup_d`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_d` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_d`
--

LOCK TABLES `AbookGroup_d` WRITE;
/*!40000 ALTER TABLE `AbookGroup_d` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_d` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_e`
--

DROP TABLE IF EXISTS `AbookGroup_e`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_e` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_e`
--

LOCK TABLES `AbookGroup_e` WRITE;
/*!40000 ALTER TABLE `AbookGroup_e` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_e` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_f`
--

DROP TABLE IF EXISTS `AbookGroup_f`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_f` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_f`
--

LOCK TABLES `AbookGroup_f` WRITE;
/*!40000 ALTER TABLE `AbookGroup_f` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_f` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_g`
--

DROP TABLE IF EXISTS `AbookGroup_g`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_g` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_g`
--

LOCK TABLES `AbookGroup_g` WRITE;
/*!40000 ALTER TABLE `AbookGroup_g` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_g` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_h`
--

DROP TABLE IF EXISTS `AbookGroup_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_h` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_h`
--

LOCK TABLES `AbookGroup_h` WRITE;
/*!40000 ALTER TABLE `AbookGroup_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_i`
--

DROP TABLE IF EXISTS `AbookGroup_i`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_i` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_i`
--

LOCK TABLES `AbookGroup_i` WRITE;
/*!40000 ALTER TABLE `AbookGroup_i` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_i` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_j`
--

DROP TABLE IF EXISTS `AbookGroup_j`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_j` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_j`
--

LOCK TABLES `AbookGroup_j` WRITE;
/*!40000 ALTER TABLE `AbookGroup_j` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_j` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_k`
--

DROP TABLE IF EXISTS `AbookGroup_k`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_k` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_k`
--

LOCK TABLES `AbookGroup_k` WRITE;
/*!40000 ALTER TABLE `AbookGroup_k` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_k` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_l`
--

DROP TABLE IF EXISTS `AbookGroup_l`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_l` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_l`
--

LOCK TABLES `AbookGroup_l` WRITE;
/*!40000 ALTER TABLE `AbookGroup_l` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_l` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_m`
--

DROP TABLE IF EXISTS `AbookGroup_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_m` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_m`
--

LOCK TABLES `AbookGroup_m` WRITE;
/*!40000 ALTER TABLE `AbookGroup_m` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_m` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_n`
--

DROP TABLE IF EXISTS `AbookGroup_n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_n` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_n`
--

LOCK TABLES `AbookGroup_n` WRITE;
/*!40000 ALTER TABLE `AbookGroup_n` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_o`
--

DROP TABLE IF EXISTS `AbookGroup_o`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_o` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_o`
--

LOCK TABLES `AbookGroup_o` WRITE;
/*!40000 ALTER TABLE `AbookGroup_o` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_o` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_other`
--

DROP TABLE IF EXISTS `AbookGroup_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_other` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_other`
--

LOCK TABLES `AbookGroup_other` WRITE;
/*!40000 ALTER TABLE `AbookGroup_other` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_other` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_p`
--

DROP TABLE IF EXISTS `AbookGroup_p`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_p` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_p`
--

LOCK TABLES `AbookGroup_p` WRITE;
/*!40000 ALTER TABLE `AbookGroup_p` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_p` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_q`
--

DROP TABLE IF EXISTS `AbookGroup_q`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_q` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_q`
--

LOCK TABLES `AbookGroup_q` WRITE;
/*!40000 ALTER TABLE `AbookGroup_q` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_q` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_r`
--

DROP TABLE IF EXISTS `AbookGroup_r`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_r` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_r`
--

LOCK TABLES `AbookGroup_r` WRITE;
/*!40000 ALTER TABLE `AbookGroup_r` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_r` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_s`
--

DROP TABLE IF EXISTS `AbookGroup_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_s` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_s`
--

LOCK TABLES `AbookGroup_s` WRITE;
/*!40000 ALTER TABLE `AbookGroup_s` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_shared`
--

DROP TABLE IF EXISTS `AbookGroup_shared`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_shared` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `Domain` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_shared`
--

LOCK TABLES `AbookGroup_shared` WRITE;
/*!40000 ALTER TABLE `AbookGroup_shared` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_shared` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_t`
--

DROP TABLE IF EXISTS `AbookGroup_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_t` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_t`
--

LOCK TABLES `AbookGroup_t` WRITE;
/*!40000 ALTER TABLE `AbookGroup_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_u`
--

DROP TABLE IF EXISTS `AbookGroup_u`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_u` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_u`
--

LOCK TABLES `AbookGroup_u` WRITE;
/*!40000 ALTER TABLE `AbookGroup_u` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_u` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_v`
--

DROP TABLE IF EXISTS `AbookGroup_v`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_v` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_v`
--

LOCK TABLES `AbookGroup_v` WRITE;
/*!40000 ALTER TABLE `AbookGroup_v` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_v` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_w`
--

DROP TABLE IF EXISTS `AbookGroup_w`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_w` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_w`
--

LOCK TABLES `AbookGroup_w` WRITE;
/*!40000 ALTER TABLE `AbookGroup_w` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_w` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_x`
--

DROP TABLE IF EXISTS `AbookGroup_x`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_x` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_x`
--

LOCK TABLES `AbookGroup_x` WRITE;
/*!40000 ALTER TABLE `AbookGroup_x` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_x` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_y`
--

DROP TABLE IF EXISTS `AbookGroup_y`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_y` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_y`
--

LOCK TABLES `AbookGroup_y` WRITE;
/*!40000 ALTER TABLE `AbookGroup_y` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_y` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookGroup_z`
--

DROP TABLE IF EXISTS `AbookGroup_z`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookGroup_z` (
  `GroupName` varchar(32) DEFAULT NULL,
  `GroupEmail` varchar(64) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` mediumint(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookGroup_z`
--

LOCK TABLES `AbookGroup_z` WRITE;
/*!40000 ALTER TABLE `AbookGroup_z` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookGroup_z` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AbookPermissions`
--

DROP TABLE IF EXISTS `AbookPermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AbookPermissions` (
  `AbookID` mediumint(8) unsigned DEFAULT NULL,
  `Account` varchar(96) NOT NULL DEFAULT '',
  `Permissions` smallint(1) DEFAULT NULL,
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Type` varchar(6) DEFAULT NULL,
  `Domain` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbookPermissions`
--

LOCK TABLES `AbookPermissions` WRITE;
/*!40000 ALTER TABLE `AbookPermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `AbookPermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_a`
--

DROP TABLE IF EXISTS `Abook_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_a` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_a`
--

LOCK TABLES `Abook_a` WRITE;
/*!40000 ALTER TABLE `Abook_a` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_b`
--

DROP TABLE IF EXISTS `Abook_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_b` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`),
  KEY `UserEmail_2` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_b`
--

LOCK TABLES `Abook_b` WRITE;
/*!40000 ALTER TABLE `Abook_b` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_b` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_c`
--

DROP TABLE IF EXISTS `Abook_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_c` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_c`
--

LOCK TABLES `Abook_c` WRITE;
/*!40000 ALTER TABLE `Abook_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_d`
--

DROP TABLE IF EXISTS `Abook_d`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_d` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_d`
--

LOCK TABLES `Abook_d` WRITE;
/*!40000 ALTER TABLE `Abook_d` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_d` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_e`
--

DROP TABLE IF EXISTS `Abook_e`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_e` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_e`
--

LOCK TABLES `Abook_e` WRITE;
/*!40000 ALTER TABLE `Abook_e` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_e` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_f`
--

DROP TABLE IF EXISTS `Abook_f`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_f` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_f`
--

LOCK TABLES `Abook_f` WRITE;
/*!40000 ALTER TABLE `Abook_f` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_f` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_g`
--

DROP TABLE IF EXISTS `Abook_g`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_g` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_g`
--

LOCK TABLES `Abook_g` WRITE;
/*!40000 ALTER TABLE `Abook_g` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_g` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_h`
--

DROP TABLE IF EXISTS `Abook_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_h` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_h`
--

LOCK TABLES `Abook_h` WRITE;
/*!40000 ALTER TABLE `Abook_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_i`
--

DROP TABLE IF EXISTS `Abook_i`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_i` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_i`
--

LOCK TABLES `Abook_i` WRITE;
/*!40000 ALTER TABLE `Abook_i` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_i` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_j`
--

DROP TABLE IF EXISTS `Abook_j`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_j` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_j`
--

LOCK TABLES `Abook_j` WRITE;
/*!40000 ALTER TABLE `Abook_j` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_j` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_k`
--

DROP TABLE IF EXISTS `Abook_k`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_k` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_k`
--

LOCK TABLES `Abook_k` WRITE;
/*!40000 ALTER TABLE `Abook_k` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_k` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_l`
--

DROP TABLE IF EXISTS `Abook_l`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_l` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_l`
--

LOCK TABLES `Abook_l` WRITE;
/*!40000 ALTER TABLE `Abook_l` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_l` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_m`
--

DROP TABLE IF EXISTS `Abook_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_m` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_m`
--

LOCK TABLES `Abook_m` WRITE;
/*!40000 ALTER TABLE `Abook_m` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_m` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_n`
--

DROP TABLE IF EXISTS `Abook_n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_n` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_n`
--

LOCK TABLES `Abook_n` WRITE;
/*!40000 ALTER TABLE `Abook_n` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_o`
--

DROP TABLE IF EXISTS `Abook_o`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_o` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_o`
--

LOCK TABLES `Abook_o` WRITE;
/*!40000 ALTER TABLE `Abook_o` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_o` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_other`
--

DROP TABLE IF EXISTS `Abook_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_other` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_other`
--

LOCK TABLES `Abook_other` WRITE;
/*!40000 ALTER TABLE `Abook_other` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_other` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_p`
--

DROP TABLE IF EXISTS `Abook_p`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_p` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_p`
--

LOCK TABLES `Abook_p` WRITE;
/*!40000 ALTER TABLE `Abook_p` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_p` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_q`
--

DROP TABLE IF EXISTS `Abook_q`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_q` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_q`
--

LOCK TABLES `Abook_q` WRITE;
/*!40000 ALTER TABLE `Abook_q` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_q` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_r`
--

DROP TABLE IF EXISTS `Abook_r`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_r` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_r`
--

LOCK TABLES `Abook_r` WRITE;
/*!40000 ALTER TABLE `Abook_r` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_r` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_s`
--

DROP TABLE IF EXISTS `Abook_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_s` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_s`
--

LOCK TABLES `Abook_s` WRITE;
/*!40000 ALTER TABLE `Abook_s` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_shared`
--

DROP TABLE IF EXISTS `Abook_shared`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_shared` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_shared`
--

LOCK TABLES `Abook_shared` WRITE;
/*!40000 ALTER TABLE `Abook_shared` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_shared` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_t`
--

DROP TABLE IF EXISTS `Abook_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_t` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_t`
--

LOCK TABLES `Abook_t` WRITE;
/*!40000 ALTER TABLE `Abook_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_u`
--

DROP TABLE IF EXISTS `Abook_u`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_u` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_u`
--

LOCK TABLES `Abook_u` WRITE;
/*!40000 ALTER TABLE `Abook_u` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_u` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_v`
--

DROP TABLE IF EXISTS `Abook_v`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_v` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_v`
--

LOCK TABLES `Abook_v` WRITE;
/*!40000 ALTER TABLE `Abook_v` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_v` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_w`
--

DROP TABLE IF EXISTS `Abook_w`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_w` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_w`
--

LOCK TABLES `Abook_w` WRITE;
/*!40000 ALTER TABLE `Abook_w` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_w` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_x`
--

DROP TABLE IF EXISTS `Abook_x`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_x` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_x`
--

LOCK TABLES `Abook_x` WRITE;
/*!40000 ALTER TABLE `Abook_x` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_x` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_y`
--

DROP TABLE IF EXISTS `Abook_y`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_y` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_y`
--

LOCK TABLES `Abook_y` WRITE;
/*!40000 ALTER TABLE `Abook_y` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_y` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Abook_z`
--

DROP TABLE IF EXISTS `Abook_z`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abook_z` (
  `UserEmail` varchar(255) DEFAULT NULL,
  `UserEmail2` varchar(255) DEFAULT NULL,
  `UserEmail3` varchar(255) DEFAULT NULL,
  `UserEmail4` varchar(255) DEFAULT NULL,
  `UserEmail5` varchar(255) DEFAULT NULL,
  `UserFirstName` varchar(128) DEFAULT NULL,
  `UserMiddleName` varchar(128) DEFAULT NULL,
  `UserLastName` varchar(128) DEFAULT NULL,
  `UserTitle` varchar(128) DEFAULT NULL,
  `UserGender` char(1) DEFAULT NULL,
  `UserDOB` datetime DEFAULT NULL,
  `UserHomeAddress` varchar(128) DEFAULT NULL,
  `UserHomeCity` varchar(128) DEFAULT NULL,
  `UserHomeState` varchar(128) DEFAULT NULL,
  `UserHomeZip` varchar(128) DEFAULT NULL,
  `UserHomeCountry` varchar(128) DEFAULT NULL,
  `UserHomePhone` varchar(128) DEFAULT NULL,
  `UserHomeMobile` varchar(128) DEFAULT NULL,
  `UserHomeFax` varchar(128) DEFAULT NULL,
  `UserURL` varchar(128) DEFAULT NULL,
  `UserWorkCompany` varchar(128) DEFAULT NULL,
  `UserWorkTitle` varchar(128) DEFAULT NULL,
  `UserWorkDept` varchar(128) DEFAULT NULL,
  `UserWorkOffice` varchar(128) DEFAULT NULL,
  `UserWorkAddress` varchar(128) DEFAULT NULL,
  `UserWorkCity` varchar(128) DEFAULT NULL,
  `UserWorkState` varchar(128) DEFAULT NULL,
  `UserWorkZip` varchar(128) DEFAULT NULL,
  `UserWorkCountry` varchar(128) DEFAULT NULL,
  `UserWorkPhone` varchar(128) DEFAULT NULL,
  `UserWorkMobile` varchar(128) DEFAULT NULL,
  `UserWorkFax` varchar(128) DEFAULT NULL,
  `UserType` varchar(16) DEFAULT NULL,
  `UserInfo` text,
  `UserPgpKey` text,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime DEFAULT NULL,
  `DateModified` varchar(12) DEFAULT NULL,
  `EntryID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `UserEmail` (`UserEmail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abook_z`
--

LOCK TABLES `Abook_z` WRITE;
/*!40000 ALTER TABLE `Abook_z` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abook_z` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Accounts`
--

DROP TABLE IF EXISTS `Accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Accounts` (
  `UserAccount` varchar(32) DEFAULT NULL,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `Type` varchar(4) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Accounts`
--

LOCK TABLES `Accounts` WRITE;
/*!40000 ALTER TABLE `Accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `Accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EmailMessage_Reply`
--

DROP TABLE IF EXISTS `EmailMessage_Reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EmailMessage_Reply` (
  `EmailMessage` longtext,
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EmailMessage_Reply`
--

LOCK TABLES `EmailMessage_Reply` WRITE;
/*!40000 ALTER TABLE `EmailMessage_Reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `EmailMessage_Reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Filter`
--

DROP TABLE IF EXISTS `Filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Filter` (
  `Header` varchar(32) NOT NULL DEFAULT '',
  `Value` varchar(255) DEFAULT NULL,
  `Score` int(2) DEFAULT NULL,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `Type` int(1) DEFAULT NULL,
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `Origin` varchar(8) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `iHeader` (`Header`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Filter`
--

LOCK TABLES `Filter` WRITE;
/*!40000 ALTER TABLE `Filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `Filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Log_Error`
--

DROP TABLE IF EXISTS `Log_Error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Log_Error` (
  `LogMsg` varchar(255) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `LogDate` (`LogDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Log_Error`
--

LOCK TABLES `Log_Error` WRITE;
/*!40000 ALTER TABLE `Log_Error` DISABLE KEYS */;
/*!40000 ALTER TABLE `Log_Error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Log_Login`
--

DROP TABLE IF EXISTS `Log_Login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Log_Login` (
  `LogMsg` varchar(64) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `LogDate` (`LogDate`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Log_Login`
--

LOCK TABLES `Log_Login` WRITE;
/*!40000 ALTER TABLE `Log_Login` DISABLE KEYS */;
INSERT INTO `Log_Login` VALUES ('Access from 209.50.226.20','2011-12-27 17:00:22','accent2001@fibertechcws.com',1),('Access from 74.105.98.242','2011-12-27 17:21:28','accent2001@fibertechcws.com',2);
/*!40000 ALTER TABLE `Log_Login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Log_RecvMail`
--

DROP TABLE IF EXISTS `Log_RecvMail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Log_RecvMail` (
  `LogMsg` varchar(64) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `LogDate` (`LogDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Log_RecvMail`
--

LOCK TABLES `Log_RecvMail` WRITE;
/*!40000 ALTER TABLE `Log_RecvMail` DISABLE KEYS */;
/*!40000 ALTER TABLE `Log_RecvMail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Log_SMS`
--

DROP TABLE IF EXISTS `Log_SMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Log_SMS` (
  `LogMsg` varchar(255) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `LogDate` (`LogDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Log_SMS`
--

LOCK TABLES `Log_SMS` WRITE;
/*!40000 ALTER TABLE `Log_SMS` DISABLE KEYS */;
/*!40000 ALTER TABLE `Log_SMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Log_SendMail`
--

DROP TABLE IF EXISTS `Log_SendMail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Log_SendMail` (
  `LogMsg` varchar(64) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `LogDate` (`LogDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Log_SendMail`
--

LOCK TABLES `Log_SendMail` WRITE;
/*!40000 ALTER TABLE `Log_SendMail` DISABLE KEYS */;
/*!40000 ALTER TABLE `Log_SendMail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Log_Spam`
--

DROP TABLE IF EXISTS `Log_Spam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Log_Spam` (
  `LogMsg` varchar(255) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `LogDate` (`LogDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Log_Spam`
--

LOCK TABLES `Log_Spam` WRITE;
/*!40000 ALTER TABLE `Log_Spam` DISABLE KEYS */;
/*!40000 ALTER TABLE `Log_Spam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Log_Virus`
--

DROP TABLE IF EXISTS `Log_Virus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Log_Virus` (
  `LogMsg` varchar(255) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  `Account` varchar(128) NOT NULL DEFAULT '',
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`),
  KEY `LogDate` (`LogDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Log_Virus`
--

LOCK TABLES `Log_Virus` WRITE;
/*!40000 ALTER TABLE `Log_Virus` DISABLE KEYS */;
/*!40000 ALTER TABLE `Log_Virus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_a`
--

DROP TABLE IF EXISTS `MailSort_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_a` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_a`
--

LOCK TABLES `MailSort_a` WRITE;
/*!40000 ALTER TABLE `MailSort_a` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_b`
--

DROP TABLE IF EXISTS `MailSort_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_b` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_b`
--

LOCK TABLES `MailSort_b` WRITE;
/*!40000 ALTER TABLE `MailSort_b` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_b` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_c`
--

DROP TABLE IF EXISTS `MailSort_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_c` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_c`
--

LOCK TABLES `MailSort_c` WRITE;
/*!40000 ALTER TABLE `MailSort_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_d`
--

DROP TABLE IF EXISTS `MailSort_d`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_d` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_d`
--

LOCK TABLES `MailSort_d` WRITE;
/*!40000 ALTER TABLE `MailSort_d` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_d` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_e`
--

DROP TABLE IF EXISTS `MailSort_e`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_e` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_e`
--

LOCK TABLES `MailSort_e` WRITE;
/*!40000 ALTER TABLE `MailSort_e` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_e` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_f`
--

DROP TABLE IF EXISTS `MailSort_f`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_f` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_f`
--

LOCK TABLES `MailSort_f` WRITE;
/*!40000 ALTER TABLE `MailSort_f` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_f` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_g`
--

DROP TABLE IF EXISTS `MailSort_g`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_g` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_g`
--

LOCK TABLES `MailSort_g` WRITE;
/*!40000 ALTER TABLE `MailSort_g` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_g` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_h`
--

DROP TABLE IF EXISTS `MailSort_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_h` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_h`
--

LOCK TABLES `MailSort_h` WRITE;
/*!40000 ALTER TABLE `MailSort_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_i`
--

DROP TABLE IF EXISTS `MailSort_i`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_i` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_i`
--

LOCK TABLES `MailSort_i` WRITE;
/*!40000 ALTER TABLE `MailSort_i` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_i` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_j`
--

DROP TABLE IF EXISTS `MailSort_j`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_j` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_j`
--

LOCK TABLES `MailSort_j` WRITE;
/*!40000 ALTER TABLE `MailSort_j` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_j` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_k`
--

DROP TABLE IF EXISTS `MailSort_k`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_k` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_k`
--

LOCK TABLES `MailSort_k` WRITE;
/*!40000 ALTER TABLE `MailSort_k` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_k` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_l`
--

DROP TABLE IF EXISTS `MailSort_l`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_l` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_l`
--

LOCK TABLES `MailSort_l` WRITE;
/*!40000 ALTER TABLE `MailSort_l` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_l` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_m`
--

DROP TABLE IF EXISTS `MailSort_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_m` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_m`
--

LOCK TABLES `MailSort_m` WRITE;
/*!40000 ALTER TABLE `MailSort_m` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_m` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_n`
--

DROP TABLE IF EXISTS `MailSort_n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_n` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_n`
--

LOCK TABLES `MailSort_n` WRITE;
/*!40000 ALTER TABLE `MailSort_n` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_o`
--

DROP TABLE IF EXISTS `MailSort_o`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_o` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_o`
--

LOCK TABLES `MailSort_o` WRITE;
/*!40000 ALTER TABLE `MailSort_o` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_o` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_other`
--

DROP TABLE IF EXISTS `MailSort_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_other` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_other`
--

LOCK TABLES `MailSort_other` WRITE;
/*!40000 ALTER TABLE `MailSort_other` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_other` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_p`
--

DROP TABLE IF EXISTS `MailSort_p`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_p` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_p`
--

LOCK TABLES `MailSort_p` WRITE;
/*!40000 ALTER TABLE `MailSort_p` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_p` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_q`
--

DROP TABLE IF EXISTS `MailSort_q`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_q` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_q`
--

LOCK TABLES `MailSort_q` WRITE;
/*!40000 ALTER TABLE `MailSort_q` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_q` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_r`
--

DROP TABLE IF EXISTS `MailSort_r`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_r` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_r`
--

LOCK TABLES `MailSort_r` WRITE;
/*!40000 ALTER TABLE `MailSort_r` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_r` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_s`
--

DROP TABLE IF EXISTS `MailSort_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_s` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_s`
--

LOCK TABLES `MailSort_s` WRITE;
/*!40000 ALTER TABLE `MailSort_s` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_t`
--

DROP TABLE IF EXISTS `MailSort_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_t` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_t`
--

LOCK TABLES `MailSort_t` WRITE;
/*!40000 ALTER TABLE `MailSort_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_u`
--

DROP TABLE IF EXISTS `MailSort_u`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_u` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_u`
--

LOCK TABLES `MailSort_u` WRITE;
/*!40000 ALTER TABLE `MailSort_u` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_u` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_v`
--

DROP TABLE IF EXISTS `MailSort_v`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_v` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_v`
--

LOCK TABLES `MailSort_v` WRITE;
/*!40000 ALTER TABLE `MailSort_v` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_v` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_w`
--

DROP TABLE IF EXISTS `MailSort_w`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_w` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_w`
--

LOCK TABLES `MailSort_w` WRITE;
/*!40000 ALTER TABLE `MailSort_w` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_w` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_x`
--

DROP TABLE IF EXISTS `MailSort_x`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_x` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_x`
--

LOCK TABLES `MailSort_x` WRITE;
/*!40000 ALTER TABLE `MailSort_x` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_x` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_y`
--

DROP TABLE IF EXISTS `MailSort_y`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_y` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_y`
--

LOCK TABLES `MailSort_y` WRITE;
/*!40000 ALTER TABLE `MailSort_y` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_y` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailSort_z`
--

DROP TABLE IF EXISTS `MailSort_z`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailSort_z` (
  `EmailAddress` varchar(64) DEFAULT NULL,
  `EmailSubject` varchar(64) DEFAULT NULL,
  `EmailFolder` varchar(32) DEFAULT NULL,
  `Account` varchar(128) DEFAULT NULL,
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailSort_z`
--

LOCK TABLES `MailSort_z` WRITE;
/*!40000 ALTER TABLE `MailSort_z` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailSort_z` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MailThreads`
--

DROP TABLE IF EXISTS `MailThreads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MailThreads` (
  `MessageID` varchar(128) DEFAULT NULL,
  `ThreadID` varchar(128) DEFAULT NULL,
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `Account` varchar(128) DEFAULT NULL,
  `ParentID` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `MessageID` (`MessageID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MailThreads`
--

LOCK TABLES `MailThreads` WRITE;
/*!40000 ALTER TABLE `MailThreads` DISABLE KEYS */;
/*!40000 ALTER TABLE `MailThreads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SpellCheck`
--

DROP TABLE IF EXISTS `SpellCheck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SpellCheck` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `Word` varchar(40) DEFAULT NULL,
  `SUnique` varchar(10) DEFAULT '0',
  `id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SpellCheck`
--

LOCK TABLES `SpellCheck` WRITE;
/*!40000 ALTER TABLE `SpellCheck` DISABLE KEYS */;
/*!40000 ALTER TABLE `SpellCheck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserPermissions`
--

DROP TABLE IF EXISTS `UserPermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserPermissions` (
  `Account` varchar(128) DEFAULT NULL,
  `DateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserPermissions`
--

LOCK TABLES `UserPermissions` WRITE;
/*!40000 ALTER TABLE `UserPermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserPermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSession`
--

DROP TABLE IF EXISTS `UserSession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSession` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `Password` varchar(64) DEFAULT NULL,
  `SessionID` varchar(64) NOT NULL DEFAULT '',
  `LastLogin` int(10) unsigned DEFAULT NULL,
  `PasswordMD5` varchar(64) DEFAULT NULL,
  `SessionData` text,
  `ChangePass` int(1) DEFAULT NULL,
  PRIMARY KEY (`Account`),
  KEY `iAccount` (`SessionID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSession`
--

LOCK TABLES `UserSession` WRITE;
/*!40000 ALTER TABLE `UserSession` DISABLE KEYS */;
INSERT INTO `UserSession` VALUES ('accent2001@fibertechcws.com','d05td05tn@r@h@','ih78o224rfhc50k6jm35lomd75',1325026697,'0b91a24b98f812343449e2eb468fb1a1','ForceImapRefresh|i:1;auth|O:4:\"Auth\":10:{s:8:\"password\";s:14:\"d05td05tn@r@h@\";s:8:\"username\";s:10:\"accent2001\";s:7:\"Account\";s:27:\"accent2001@fibertechcws.com\";s:9:\"SessionID\";s:26:\"ih78o224rfhc50k6jm35lomd75\";s:8:\"pop3host\";s:16:\"fibertechcws.com\";s:5:\"debug\";N;s:15:\"pgpGenerateKeys\";N;s:11:\"pgpPassword\";N;s:4:\"mode\";N;s:13:\"authenticated\";b:1;}referer|s:13:\"/showmail.php\";',NULL);
/*!40000 ALTER TABLE `UserSession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_a`
--

DROP TABLE IF EXISTS `UserSettings_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_a` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_a`
--

LOCK TABLES `UserSettings_a` WRITE;
/*!40000 ALTER TABLE `UserSettings_a` DISABLE KEYS */;
INSERT INTO `UserSettings_a` VALUES ('accent2001@fibertechcws.com','id',' ',1200,'standard','','25',0,0,1,'accent2001@fibertechcws.com',NULL,'Verdana',1,'simple',1,'#EBE9E4','#F8FBFD','#000000','#000033','#FFFFFF','#000033','#FBFBFB','#E2E7FA','imap','#FAFAFA','sql',NULL,'english',NULL,'#F3F3F3','#FFFFFF','#002675','#DFEAF4','imgs/bluegrad.gif',0,'localhost',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'%e/%m/%y','%l:%M %p',1,'UTF-8',1,'1',0);
/*!40000 ALTER TABLE `UserSettings_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_b`
--

DROP TABLE IF EXISTS `UserSettings_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_b` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_b`
--

LOCK TABLES `UserSettings_b` WRITE;
/*!40000 ALTER TABLE `UserSettings_b` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_b` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_c`
--

DROP TABLE IF EXISTS `UserSettings_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_c` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_c`
--

LOCK TABLES `UserSettings_c` WRITE;
/*!40000 ALTER TABLE `UserSettings_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_d`
--

DROP TABLE IF EXISTS `UserSettings_d`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_d` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_d`
--

LOCK TABLES `UserSettings_d` WRITE;
/*!40000 ALTER TABLE `UserSettings_d` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_d` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_e`
--

DROP TABLE IF EXISTS `UserSettings_e`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_e` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_e`
--

LOCK TABLES `UserSettings_e` WRITE;
/*!40000 ALTER TABLE `UserSettings_e` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_e` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_f`
--

DROP TABLE IF EXISTS `UserSettings_f`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_f` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_f`
--

LOCK TABLES `UserSettings_f` WRITE;
/*!40000 ALTER TABLE `UserSettings_f` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_f` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_g`
--

DROP TABLE IF EXISTS `UserSettings_g`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_g` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_g`
--

LOCK TABLES `UserSettings_g` WRITE;
/*!40000 ALTER TABLE `UserSettings_g` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_g` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_h`
--

DROP TABLE IF EXISTS `UserSettings_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_h` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_h`
--

LOCK TABLES `UserSettings_h` WRITE;
/*!40000 ALTER TABLE `UserSettings_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_i`
--

DROP TABLE IF EXISTS `UserSettings_i`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_i` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_i`
--

LOCK TABLES `UserSettings_i` WRITE;
/*!40000 ALTER TABLE `UserSettings_i` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_i` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_j`
--

DROP TABLE IF EXISTS `UserSettings_j`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_j` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_j`
--

LOCK TABLES `UserSettings_j` WRITE;
/*!40000 ALTER TABLE `UserSettings_j` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_j` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_k`
--

DROP TABLE IF EXISTS `UserSettings_k`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_k` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_k`
--

LOCK TABLES `UserSettings_k` WRITE;
/*!40000 ALTER TABLE `UserSettings_k` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_k` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_l`
--

DROP TABLE IF EXISTS `UserSettings_l`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_l` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_l`
--

LOCK TABLES `UserSettings_l` WRITE;
/*!40000 ALTER TABLE `UserSettings_l` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_l` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_m`
--

DROP TABLE IF EXISTS `UserSettings_m`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_m` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_m`
--

LOCK TABLES `UserSettings_m` WRITE;
/*!40000 ALTER TABLE `UserSettings_m` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_m` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_n`
--

DROP TABLE IF EXISTS `UserSettings_n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_n` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_n`
--

LOCK TABLES `UserSettings_n` WRITE;
/*!40000 ALTER TABLE `UserSettings_n` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_o`
--

DROP TABLE IF EXISTS `UserSettings_o`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_o` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_o`
--

LOCK TABLES `UserSettings_o` WRITE;
/*!40000 ALTER TABLE `UserSettings_o` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_o` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_other`
--

DROP TABLE IF EXISTS `UserSettings_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_other` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_other`
--

LOCK TABLES `UserSettings_other` WRITE;
/*!40000 ALTER TABLE `UserSettings_other` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_other` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_p`
--

DROP TABLE IF EXISTS `UserSettings_p`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_p` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_p`
--

LOCK TABLES `UserSettings_p` WRITE;
/*!40000 ALTER TABLE `UserSettings_p` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_p` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_q`
--

DROP TABLE IF EXISTS `UserSettings_q`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_q` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_q`
--

LOCK TABLES `UserSettings_q` WRITE;
/*!40000 ALTER TABLE `UserSettings_q` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_q` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_r`
--

DROP TABLE IF EXISTS `UserSettings_r`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_r` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_r`
--

LOCK TABLES `UserSettings_r` WRITE;
/*!40000 ALTER TABLE `UserSettings_r` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_r` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_s`
--

DROP TABLE IF EXISTS `UserSettings_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_s` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_s`
--

LOCK TABLES `UserSettings_s` WRITE;
/*!40000 ALTER TABLE `UserSettings_s` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_t`
--

DROP TABLE IF EXISTS `UserSettings_t`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_t` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_t`
--

LOCK TABLES `UserSettings_t` WRITE;
/*!40000 ALTER TABLE `UserSettings_t` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_t` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_u`
--

DROP TABLE IF EXISTS `UserSettings_u`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_u` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_u`
--

LOCK TABLES `UserSettings_u` WRITE;
/*!40000 ALTER TABLE `UserSettings_u` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_u` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_v`
--

DROP TABLE IF EXISTS `UserSettings_v`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_v` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_v`
--

LOCK TABLES `UserSettings_v` WRITE;
/*!40000 ALTER TABLE `UserSettings_v` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_v` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_w`
--

DROP TABLE IF EXISTS `UserSettings_w`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_w` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_w`
--

LOCK TABLES `UserSettings_w` WRITE;
/*!40000 ALTER TABLE `UserSettings_w` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_w` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_x`
--

DROP TABLE IF EXISTS `UserSettings_x`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_x` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_x`
--

LOCK TABLES `UserSettings_x` WRITE;
/*!40000 ALTER TABLE `UserSettings_x` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_x` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_y`
--

DROP TABLE IF EXISTS `UserSettings_y`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_y` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_y`
--

LOCK TABLES `UserSettings_y` WRITE;
/*!40000 ALTER TABLE `UserSettings_y` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_y` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserSettings_z`
--

DROP TABLE IF EXISTS `UserSettings_z`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSettings_z` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `MboxOrder` varchar(12) DEFAULT NULL,
  `RealName` varchar(40) DEFAULT NULL,
  `Refresh` smallint(5) DEFAULT NULL,
  `EmailHeaders` varchar(8) DEFAULT NULL,
  `TimeZone` varchar(32) DEFAULT NULL,
  `MsgNum` varchar(4) DEFAULT NULL,
  `EmptyTrash` smallint(1) DEFAULT NULL,
  `NewWindow` smallint(1) DEFAULT NULL,
  `HtmlEditor` smallint(1) DEFAULT NULL,
  `ReplyTo` varchar(255) DEFAULT NULL,
  `Signature` text,
  `FontStyle` varchar(21) DEFAULT NULL,
  `LeaveMsgs` smallint(1) DEFAULT NULL,
  `LoginType` varchar(12) DEFAULT NULL,
  `Advanced` smallint(1) DEFAULT NULL,
  `PrimaryColor` varchar(7) DEFAULT NULL,
  `SecondaryColor` varchar(7) DEFAULT NULL,
  `LinkColor` varchar(7) DEFAULT NULL,
  `VlinkColor` varchar(7) DEFAULT NULL,
  `BgColor` varchar(7) DEFAULT NULL,
  `TextColor` varchar(7) DEFAULT NULL,
  `HeaderColor` varchar(7) DEFAULT NULL,
  `HeadColor` varchar(7) DEFAULT NULL,
  `MailType` varchar(4) DEFAULT NULL,
  `ThirdColor` varchar(7) DEFAULT NULL,
  `Mode` varchar(4) DEFAULT NULL,
  `Service` smallint(1) DEFAULT NULL,
  `Language` varchar(10) DEFAULT NULL,
  `StartPage` char(1) DEFAULT NULL,
  `OnColor` varchar(7) DEFAULT NULL,
  `OffColor` varchar(7) DEFAULT NULL,
  `TextHeadColor` varchar(7) DEFAULT NULL,
  `SelectColor` varchar(7) DEFAULT NULL,
  `TopBg` varchar(24) DEFAULT NULL,
  `AutoTrash` tinyint(1) DEFAULT NULL,
  `MailServer` varchar(64) DEFAULT NULL,
  `MailAuth` tinyint(1) DEFAULT NULL,
  `PGPenable` smallint(1) DEFAULT NULL,
  `PGPappend` smallint(1) DEFAULT NULL,
  `PGPsign` smallint(1) DEFAULT NULL,
  `SpamTreatment` varchar(10) DEFAULT NULL,
  `AbookTrusted` smallint(1) DEFAULT NULL,
  `AntiVirus` smallint(1) DEFAULT NULL,
  `PassCode` varchar(64) DEFAULT NULL,
  `DateFormat` varchar(8) DEFAULT NULL,
  `TimeFormat` varchar(8) DEFAULT NULL,
  `AutoComplete` smallint(1) DEFAULT NULL,
  `EmailEncoding` varchar(16) DEFAULT NULL,
  `DisplayImages` smallint(1) DEFAULT NULL,
  `Ajax` char(1) DEFAULT NULL,
  `UseSSL` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserSettings_z`
--

LOCK TABLES `UserSettings_z` WRITE;
/*!40000 ALTER TABLE `UserSettings_z` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserSettings_z` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `Account` varchar(128) NOT NULL DEFAULT '',
  `PasswordQuestion` varchar(64) DEFAULT NULL,
  `OtherEmail` varchar(128) DEFAULT NULL,
  `FirstName` varchar(96) DEFAULT NULL,
  `LastName` varchar(96) DEFAULT NULL,
  `BirthDay` smallint(2) DEFAULT NULL,
  `BirthMonth` smallint(2) DEFAULT NULL,
  `BirthYear` smallint(4) DEFAULT NULL,
  `Gender` char(1) DEFAULT NULL,
  `Industry` varchar(42) DEFAULT NULL,
  `Occupation` varchar(40) DEFAULT NULL,
  `Address` varchar(96) DEFAULT NULL,
  `City` varchar(96) DEFAULT NULL,
  `PostCode` varchar(36) DEFAULT NULL,
  `State` varchar(24) DEFAULT NULL,
  `Country` varchar(32) DEFAULT NULL,
  `DateModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateCreate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `TelHome` varchar(16) DEFAULT NULL,
  `FaxHome` varchar(16) DEFAULT NULL,
  `TelWork` varchar(16) DEFAULT NULL,
  `FaxWork` varchar(16) DEFAULT NULL,
  `TelMobile` varchar(16) DEFAULT NULL,
  `TelPager` varchar(16) DEFAULT NULL,
  `Ugroup` varchar(16) DEFAULT NULL,
  `UserStatus` int(1) DEFAULT NULL,
  `MailDir` varchar(255) DEFAULT NULL,
  `Forward` varchar(128) DEFAULT NULL,
  `AutoReply` text,
  `UserQuota` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAccount` (`Account`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES ('accent2001@fibertechcws.com',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2011-12-27 22:00:18','2011-12-27 22:00:18',1,NULL,NULL,NULL,NULL,NULL,NULL,'Default',0,NULL,NULL,NULL,51200);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-02-21 16:53:31
